# Selectbox
Selectbox is a simple and flexible jQuery plugin that helps you fully style your `<select>` elements, and keeps them readable and accessible to all.

## Install and use
Simply download the plugin through npm or bower. You can then use it with your favorite environment (the plugin supports CommonJS, AMD and global jQuery).

```js
// CommonJS
var $ = require('jquery');
require('selectbox');

$('select').selectbox();

// AMD
require(['jquery', 'selectbox'], function($) {

  $('select').selectbox();
});

// Global (include both jquery and selectbox JS files in your html)
$('select').selectbox();
```

You can then style the generated elements (see the examples for basic styling).

## Options

#### html (boolean)
*default : false*
You can switch it to true to insert the options' labels and option groups' labels as html instead of plain text (be careful of injections !). You can add a `data-original-label` attribute on the options to use as a new label.

#### onParseOption (function)
A function that takes the option jQuery object as a parameter and must return a string that will be injected as the option or option group label.

#### width (string)
*default : 'min-width'*
Tells selectbox which CSS property it should use to set the select width : `min-width` or `width`. Use "manual" if you want to set the width with CSS.

#### isResponsive (boolean)
*default : true*
Tells selectbox to switch between two rendering modes depending on the viewport's width.

#### mobileBreakpoint (int)
*default : 768*
Under this viewport's width, selectbox will switch to mobile display of the options' list.

#### desktopAnimation (string|function)
*default : 'toggle'*
The animation function to use when selectbox is in desktop mode. (see animation section).

#### mobileAnimation (string|function)
*default : 'mobile'*
The animation function to use when selectbox is in mobile mode. (see animation section).

#### optionsTitleLabel (string)
*default : null*
If specified, it will be displayed on top of the options' list. If not, selectbox will retrieve the `<label>` associated with the source `<select>` element.

#### cancelBtnLabel (string)
*default : 'Cancel'*
The label of the cancel button that closes the options list.

#### containerClass (string)
*default : 'selectbox'*

#### buttonClass (string)
*default : 'selectbox-button'*

#### listBoxClass (string)
*default : 'selectbox-list-box'*

#### listClass (string)
*default : 'selectbox-list'*

#### listItemClass (string)
*default : 'selectbox-list-item'*

#### groupLabelClass (string)
*default : 'selectbox-group-label'*

#### groupItemClass (string)
*default : 'selectbox-group-item'*

#### dividerClass (string)
*default : 'selectbox-list-divider'*

#### optionsTitleClass (string)
*default : 'selectbox-items-title'*

#### cancelBtnClass (string)
*default : 'selectbox-cancel'*

## API
The API can be called through the plugin, or by retrieving it with the jQuery `data()` method.

```js
// with the plugin
$element.selectbox('method_name', {params});

// with the data
var api = $element.data('selectbox');
api.method_name(params);
```

### Methods

#### open()
Opens the options list.

#### close()
Closes the options list.

#### setValue(value)
Sets the value of the select.

#### disable()
Disables the select.

#### enable()
Enables the select.

#### isDisabled() => (boolean)
Returns if the select is disabled.

#### refresh()
Updates all options and option groups. To call when the source `<select>` has changed.

#### options(options)
Changes the settings and then refresh the selectbox.

#### destroy()
Removes the plugin and restore the source `<select>`.

### Useful properties
When using the API through the data method, you can access these properties.

#### settings (object)
The list of the selectbox settings.

#### $element (jQuery object)
The source `<select>`.

#### $container (jQuery object)

#### $button (jQuery object)

#### $list (jquery object)

## Animations
Selectbox comes with two default animations : 'toggle' and 'mobile'. Those reproduce basic rendering of a native `<select>` element.

You can also create your own function to call when opening or closing the selectbox, and pass them through options (see above).

```js
$('select').selectbox({
  desktopAnimation: function($selectbox, api, show) {

    // Code here
  },
  mobileAnimation: function($selectbox, api, show) {

    // Code here
  }
});
```

`$selectbox` refers to the main selectbox container as a jQuery object, `show` is a boolean (true if the selectbox is opening, false if it's closing), and see above for what you can access from the `api` parameter.

If you need to store the function, you can add it to the `$.fn.selectbox.animations`, and refers to it through its name when passing options.

```js
$.fn.selectbox.animations.customAnimation = function($selectbox, api, show) {

  // Code here
};

$('select').selectbox({
  desktopAnimation: 'customAnimation'
});
```
